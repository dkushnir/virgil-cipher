namespace SimpleTest.Playground
{
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using Handler;

    public class LocalDropboxFolder
    {
        private readonly ILocalEventListener eventListener;
        public ObservableCollection<LocalFile> Files { get; } = new ObservableCollection<LocalFile>();

        public string Folder { get; }

        public LocalDropboxFolder(string folder, ILocalEventListener eventListener)
        {
            this.eventListener = eventListener;
            this.Folder = folder;
            ServerPath.BaseFolder = folder;
        }

        public void Init()
        {
            var paths = Directory.EnumerateFiles(Folder, "*", SearchOption.AllDirectories);

            Files.Clear();
            Console.WriteLine("+ ================================================ +");
            foreach (var path in paths)
            {
                var localFileItem = new LocalFile(path);
                Files.Add(localFileItem);
                Console.WriteLine(localFileItem);
            }

            eventListener.OnInitialized(this);
        }

        public void Handle(FileSystemEventArgs args)
        {
            try
            {
                switch (args.ChangeType)
                {
                    case WatcherChangeTypes.Created:
                        {
                            this.Files.Add(new LocalFile(args.FullPath));
                            this.eventListener.On(new FileCreatedLocallyEvent(new LocalFile(args.FullPath)));
                            Console.WriteLine($"Added: {args.FullPath}");
                            break;
                        }
                    case WatcherChangeTypes.Deleted:
                        {
                            var toDelete = this.Files.FirstOrDefault(it => string.Equals(it.LocalPath, args.FullPath, StringComparison.InvariantCultureIgnoreCase));
                            this.Files.Remove(toDelete);
                            this.eventListener.On(new FileDeletedLocallyEvent(new LocalFile(args.FullPath)));
                            Console.WriteLine($"Deleted: {args.FullPath}");
                            break;
                        }
                    case WatcherChangeTypes.Changed:
                        {
                            this.eventListener.On(new FileChangedLocallyEvent(new LocalFile(args.FullPath)));
                            Console.WriteLine($"Changed: {args.FullPath}");
                            break;
                        }
                    case WatcherChangeTypes.Renamed:
                        {
                            var renamedEventArgs = ((RenamedEventArgs)args);

                            var toDelete = this.Files.FirstOrDefault(it => string.Equals(it.LocalPath, renamedEventArgs.OldFullPath, StringComparison.InvariantCultureIgnoreCase));
                            this.Files.Remove(toDelete);
                            this.Files.Add(new LocalFile(args.FullPath));

                            if (!renamedEventArgs.OldFullPath.EndsWith(LocalDropboxFolderWatcher.TemporaryFileExtension))
                            {
                                this.eventListener.On(new FileMovedLocallyEvent
                                {
                                    NewLocalPath = renamedEventArgs.FullPath,
                                    NewServerPath = renamedEventArgs.FullPath.ToServerPath(),
                                    LocalPath = renamedEventArgs.OldFullPath,
                                    ServerPath = renamedEventArgs.OldFullPath.ToServerPath()
                                });
                            }

                            Console.WriteLine($"Moved: {renamedEventArgs.OldFullPath} To : {renamedEventArgs.FullPath}");
                            break;
                        }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
            }
        }
    }
}