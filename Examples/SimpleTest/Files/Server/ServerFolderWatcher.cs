namespace SimpleTest.Playground
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using DropNetRT;
    using DropNetRT.Models;
    using Virgil.DropBox.Client;

    public class ServerFolderWatcher
    {
        private readonly ServerFolder serverFolder;
        private readonly DropNetClient client;
        
        private readonly CancellationToken cancellationToken;
        private readonly CancellationTokenSource cancellationTokenSource;
        
        public ServerFolderWatcher(DropNetClient client, ServerFolder serverFolder)
        {
            this.serverFolder = serverFolder;
            this.client = client;
            this.cancellationTokenSource = new CancellationTokenSource();
            this.cancellationToken = this.cancellationTokenSource.Token;
        }

        private async Task Init()
        {
            var folderStructure = await this.GetDelta("");
            serverFolder.Init(folderStructure);
        }

        private async Task<Delta> GetDelta(string cursor)
        {
            var result = new List<DeltaPage>();

            var dp = await this.client.GetDelta(cursor, this.cancellationToken);
            result.Add(dp);
            while (dp.Has_More)
            {
                dp = await this.client.GetDelta(cursor, this.cancellationToken);
                result.Add(dp);
            }
            
            return new Delta(result, dp.Cursor); 
        }

        public async Task Start()
        {
            await Init();

            Task.Factory.StartNew(() => this.CloudWatcher(this.cancellationToken), TaskCreationOptions.LongRunning);
        }

        public void Stop()
        {
            this.cancellationTokenSource.Cancel();
        }

        private async void CloudWatcher(CancellationToken token)
        {
            try
            {
                var delta = await this.GetDelta(Settings.Default.DeltaCursor);
                
                await this.serverFolder.HandleDelta(delta);

                Settings.Default.DeltaCursor = delta.Cursor;
                Settings.Default.Save();

                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }

                    var longpollDeltaResult = await this.client.GetLongpollDelta(Settings.Default.DeltaCursor, this.cancellationToken);
                    if (longpollDeltaResult.Changes)
                    {
                        delta = await this.GetDelta(Settings.Default.DeltaCursor);

                        await this.serverFolder.HandleDelta(delta);

                        Settings.Default.DeltaCursor = delta.Cursor;
                        Settings.Default.Save();
                    }
                    await Task.Delay(TimeSpan.FromSeconds(longpollDeltaResult.Backoff), this.cancellationToken);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}