﻿namespace SimpleTest.Playground
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Windows.Threading;
    using Annotations;
    using Handler;

    public class MainModel : INotifyPropertyChanged
    {
        private BigBrother bigBrother;
        private LocalDropboxFolder localDropboxFolder;
        private ServerFolder serverFolder;
        private List<Operation> operations;
        private List<ServerFile> serverFiles;
        private List<LocalFile> localFiles;

        public List<LocalFile> LocalFiles
        {
            get { return this.localFiles; }
            set
            {
                if (Equals(value, this.localFiles)) return;
                this.localFiles = value;
                this.OnPropertyChanged();
            }
        }

        public List<ServerFile> ServerFiles
        {
            get { return this.serverFiles; }
            set
            {
                if (Equals(value, this.serverFiles)) return;
                this.serverFiles = value;
                this.OnPropertyChanged();
            }
        }

        public List<Operation> Operations
        {
            get { return this.operations; }
            set
            {
                if (Equals(value, this.operations)) return;
                this.operations = value;
                this.OnPropertyChanged();
            }
        }


        public MainModel()
        {
            
        }

        public MainModel(BigBrother bigBrother, LocalDropboxFolder localDropboxFolder, ServerFolder serverFolder)
        {
            this.bigBrother = bigBrother;
            this.localDropboxFolder = localDropboxFolder;
            this.serverFolder = serverFolder;
            

            LocalFiles = localDropboxFolder.Files.ToList();
            ServerFiles = serverFolder.Files.ToList();
            Operations = bigBrother.Operations.ToList();

            localDropboxFolder.Files.CollectionChanged += (sender, args) =>
            {
                Dispatcher.CurrentDispatcher.Invoke(() => LocalFiles = localDropboxFolder.Files.ToList());
            };

            serverFolder.Files.CollectionChanged += (sender, args) =>
            {
                Dispatcher.CurrentDispatcher.Invoke(() => ServerFiles = serverFolder.Files.ToList());
            };

            bigBrother.Operations.CollectionChanged += (sender, args) =>
            {
                Dispatcher.CurrentDispatcher.Invoke(() => Operations = bigBrother.Operations.ToList());
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
