namespace SimpleTest.Playground.Handler
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Caching;
    using System.Threading.Tasks;
    using DropNetRT.Models;

    public class RecentEventsStore 
    {
        public void PutRecentlyUploadedPath(string localPath)
        {
            MemoryCache.Default.Add(localPath.ToLowerInvariant(), Guid.NewGuid().ToString(), DateTimeOffset.Now.AddSeconds(15));
        }

        public bool PathWasRecentlyUploaded(string localPath)
        {
            return MemoryCache.Default.Get(localPath.ToLowerInvariant()) != null;
        }
        
        public void RemovePath(string localPath)
        {
            var key = localPath.ToLowerInvariant();
            MemoryCache.Default.Remove(key); 
        }
    }

    public class BigBrother : ILocalEventListener, IServerEventListener
    {
        private readonly ICloudStore cloudStore;
        public ObservableCollection<Operation> Operations = new ObservableCollection<Operation>();
        private readonly ConcurrentQueue<Operation> operationsInternal = new ConcurrentQueue<Operation>();

        readonly TaskCompletionSource<int> tcs1 = new TaskCompletionSource<int>();
        readonly TaskCompletionSource<int> tcs2 = new TaskCompletionSource<int>();
        
        private readonly RecentEventsStore recentEventsStore;

        private readonly Dictionary<string, List<Task>> unitOfWorkQueue = new Dictionary<string, List<Task>>(); 

        public BigBrother(ICloudStore cloudStore)
        {
            this.recentEventsStore = new RecentEventsStore();
            this.cloudStore = cloudStore;

            Task.WhenAll(tcs2.Task, tcs1.Task).ContinueWith(r => InitStorage());
            Task.Factory.StartNew(() => this.Consumer(), TaskCreationOptions.LongRunning);
        }

        private void InitStorage()
        {
            //Console.WriteLine(localFileItems);
            //Console.WriteLine(serverFileItems);

            //var local = this.localFileItems.Select(it => it.LocalPath.ToServerPath().ToLowerInvariant()).ToArray();
            //var server = this.serverFileItems.Select(it => it.Path.ToLowerInvariant()).ToArray();

            ////var filesToUpload = local.Except(server).ToList();
            //var filesToDownload = server.Except(local).ToList();

            ////foreach (var file in filesToUpload)
            ////{
            ////    Add(new UploadFileToServerOperation(file, cloudStore));
            ////}

            //foreach (var file in filesToDownload)
            //{
            //    Add(new DownloadFileFromServer(file, cloudStore));
            //}

            //var maybeChanges = serverDelta
            //    .Where(it => it.MetaData.IsDirectory == false && it.MetaData.IsDeleted == false)
            //    .Select(it => new {Path = it.Path.ToLowerInvariant(), it.MetaData})
            //    .Where(it => !filesToDownload.Contains(it.Path))
            //    .ToList();

            //var hashEntries = this.logRepository.GetAll();

            //foreach (var serverEntry in maybeChanges)
            //{
            //    var localEntry = hashEntries.FirstOrDefault(it => it.ServerPath == serverEntry.Path);

            //    if (localEntry == null) continue;

            //    if (localEntry.ServerModified != serverEntry.MetaData.UTCDateClientMtime)
            //    {
            //        this.Add(new DownloadFileFromServer(serverEntry.Path, this.cloudStore));
            //    }
            //}
        }

        public void On(FileCreatedLocallyEvent @event)
        {
            this.Add(new UploadFileToServerOperation(@event, this.cloudStore, recentEventsStore));
        }

        public void On(FileDeletedLocallyEvent @event)
        {
            this.Add(new DeleteFileOnServerOperation(@event, this.cloudStore));
        }

        public void On(FileChangedLocallyEvent @event)
        {
            this.Add(new UploadChangedFileToServerOperation(@event, this.cloudStore, recentEventsStore));
        }

        public void On(FileMovedLocallyEvent @event)
        {
            this.Add(new RenameFileOnServerOperation(@event, this.cloudStore));
        }

        public void Start(string cursor)
        {
            unitOfWorkQueue[cursor] = new List<Task>();
        }

        public void Add(ServerFileAddedEvent @event)
        {
            if (!recentEventsStore.PathWasRecentlyUploaded(@event.LocalPath))
                this.AddForUoW(new DownloadFileFromServer(@event, cloudStore));
            else
            {
                recentEventsStore.RemovePath(@event.LocalPath);
            }
        }

        public void Add(ServerFileDeletedEvent @event)
        {
            this.AddForUoW(new DeleteFileLocally(@event));
        }

        public void Add(ServerFileChangedEvent @event)
        {
            if (!recentEventsStore.PathWasRecentlyUploaded(@event.LocalPath))
                this.AddForUoW(new DownloadFileFromServer(@event, cloudStore));
            else
            {
                recentEventsStore.RemovePath(@event.LocalPath);
            }
        }

        public async Task WaitForAll(string cursor)
        {
            try
            {
                var array = this.unitOfWorkQueue[cursor].ToArray();
                await Task.WhenAll(array);
            }
            finally
            {
                this.unitOfWorkQueue[cursor] = null;
            }
        }

        public void OnInitialized(LocalDropboxFolder dropboxFolder)
        {
            tcs1.SetResult(1);
        }

        public void OnInitialized(ServerFolder folder, List<DeltaEntry> entries)
        {
            tcs2.SetResult(2);
        }

        private void Add(Operation operation)
        {
            Operations.Add(operation);
            operationsInternal.Enqueue(operation);
        }

        private void AddForUoW(ServerInitiatedOperation operation)
        {
            unitOfWorkQueue[operation.Cursor].Add(operation.OnCompleted());
            Add(operation);
        }

        private async void Consumer()
        {
            while (true)
            {
                Operation operation;
                if (operationsInternal.TryDequeue(out operation))
                {
                    try
                    {
                        await operation.Execute();
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.ToString());
                    }
                    finally
                    {
                        Operations.Remove(operation);
                    }
                }
                else
                {
                    await Task.Delay(500);
                }
            }
        }
    }
}