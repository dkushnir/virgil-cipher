namespace SimpleTest.Playground.Handler
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DropNetRT.Models;

    public interface ILocalEventListener
    {
        void On(FileCreatedLocallyEvent @event);
        void On(FileDeletedLocallyEvent @event);
        void On(FileChangedLocallyEvent @event);
        void On(FileMovedLocallyEvent @event);
        void OnInitialized(LocalDropboxFolder dropboxFolder);
    }

    public interface IServerEventListener
    {
        void Start(string cursor);
        void Add(ServerFileAddedEvent @event);
        void Add(ServerFileDeletedEvent @event);
        void Add(ServerFileChangedEvent @event);
        Task WaitForAll(string cursor);

        void OnInitialized(ServerFolder serverFileItems, List<DeltaEntry> entries);
    }

}