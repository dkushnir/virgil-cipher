namespace SimpleTest.Playground
{
    public static class ServerPath
    {
        public static string BaseFolder { get; set; }

        public static string ToServerPath(this string fileSystemPath)
        {
            var intermed = fileSystemPath.Replace(BaseFolder, "/");
            return intermed.Replace("\\", "/");
        }

        public static string ToLocalPath(this string serverPath)
        {
            return BaseFolder + serverPath.Replace("/", "\\").Replace(@"\\", @"\");
        }
    }
}