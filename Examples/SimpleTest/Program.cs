﻿namespace SimpleTest
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using Lib;
    using Playground;
    using Playground.Handler;
    using Virgil.DropBox.Client;

    public class MockedLocalEventListener : ILocalEventListener
    {
        public void On(FileCreatedLocallyEvent @event)
        {
            
        }

        public void On(FileDeletedLocallyEvent @event)
        {
            
        }

        public void On(FileChangedLocallyEvent @event)
        {
            
        }

        public void On(FileMovedLocallyEvent @event)
        {
            
        }

        public void OnInitialized(LocalDropboxFolder dropboxFolder)
        {
            
        }
    }

    internal class Program
    {
        [STAThread]
        private static void Main()
        {
            try
            {
                var dropBoxfolder = new LocalDropboxFolder(@"C:\Users\dmitr_000\Dropbox\VirgilCrypto\", new MockedLocalEventListener());
                var watcher = new LocalDropboxFolderWatcher(dropBoxfolder);
                watcher.Start();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

            Console.ReadLine();
        }
    }
}
