﻿using System.Windows;

namespace Virgil.DropBox.Crypto
{
    using Encryption;
    using Files;
    using Files.Handler;
    using Files.Local;
    using Files.Server;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override async void OnStartup(StartupEventArgs e)
        {
            var dropNetClient = await new DropNetClientFactory().GetInstance();
            var credentials = await Keystore.Get();

            var cs = new CloudStore(dropNetClient, credentials);

            var bigBrother = new BigBrother(cs);

            var localFolder = new LocalFolder(@"D:\ocr-export\tests2\", bigBrother);
            var localFolderWatcher = new LocalFolderWatcher(localFolder);
            localFolderWatcher.Start();

            var localDropBoxFolder = new LocalDropboxFolder(@"C:\Users\dmitr_000\Dropbox\", );

            var mainWindow = new Files.MainWindow
            {
                DataContext = new MainModel(bigBrother, localFolder, serverFolder)
            };

            mainWindow.Show();
        }
    }
}
