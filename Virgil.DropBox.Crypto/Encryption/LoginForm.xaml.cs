﻿namespace Virgil.DropBox.Crypto.Encryption
{
    using System;
    using System.Windows;
    using System.Windows.Navigation;

    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class LoginForm : Window
    {
        private readonly string uri;
        private const string RedirectUri = "http://localhost:5000/Home/Auth";
        
        public LoginForm(string uri)
        {
            this.uri = uri;
            InitializeComponent();
            this.Browser.Navigate(this.uri);
        }

        public string AccessToken { get; private set; }

        public string UserId { get; private set; }

        public bool Result { get; private set; }

        private void BrowserNavigating(object sender, NavigatingCancelEventArgs e)
        {
            if (!e.Uri.ToString().StartsWith(RedirectUri, StringComparison.OrdinalIgnoreCase))
            {
                // we need to ignore all navigation that isn't to the redirect uri.
                return;
            }

            try
            {
                this.Result = true;
            }
            catch (ArgumentException)
            {
                // There was an error in the URI passed to ParseTokenFragment
            }
            finally
            {
                e.Cancel = true;
                this.Close();
            }
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
