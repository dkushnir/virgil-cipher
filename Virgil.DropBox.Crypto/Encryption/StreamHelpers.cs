namespace Virgil.DropBox.Crypto.Encryption
{
    using System;
    using System.Threading.Tasks;

    public static class StreamHelpers
    {
        public static async Task<byte[]> TryReadExactly(this System.IO.Stream stream, int count, byte[] buffer)
        {
            int offset = 0;
            while (offset < count)
            {
                int read = await stream.ReadAsync(buffer, offset, count - offset);
                offset += read;
                if (read == 0)
                {
                    var result = new byte[offset];
                    Array.Copy(buffer, result, offset);
                    return result;
                }
            }
            
            return buffer;
        }
    }
}