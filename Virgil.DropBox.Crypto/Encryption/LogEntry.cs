namespace Virgil.DropBox.Crypto.Encryption
{
    using System;

    public class LogEntry
    {
        public string LocalPath { get; set; }
        public string ServerPath { get; set; }
        public DateTime LocalModified { get; set; }
        public DateTime ServerModified { get; set; }
        public Hashes Hashes { get; set; }
    }
}