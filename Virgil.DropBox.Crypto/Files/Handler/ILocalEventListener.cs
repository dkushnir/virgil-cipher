namespace Virgil.DropBox.Crypto.Files.Handler
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DropNetRT.Models;
    using Local;
    using Server;

    public interface ILocalEventListener
    {
        void On(FileCreatedLocallyEvent @event);
        void On(FileDeletedLocallyEvent @event);
        void On(FileChangedLocallyEvent @event);
        void On(FileMovedLocallyEvent @event);
        void OnInitialized(LocalFolder folder);
    }

    public interface IServerEventListener
    {
        void Start(string cursor);
        void Add(ServerFileAddedEvent @event);
        void Add(ServerFileDeletedEvent @event);
        void Add(ServerFileChangedEvent @event);
        Task WaitForAll(string cursor);

        void OnInitialized(ServerFolder serverFileItems, List<DeltaEntry> entries);
    }

}