namespace Virgil.DropBox.Crypto.Files.Handler
{
    using System;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using Annotations;

    public class Operation 
    {
        protected CancellationTokenSource Cts = new CancellationTokenSource();
        
        public string Title { get; set; }
        public bool IsCompleted { get; set; }
        public int Progress { get; set; }
        
        public virtual Task Execute()
        {
            throw new NotImplementedException();
        }

        public virtual void Cancel()
        {
            this.Cts.Cancel();
        }

        public override string ToString()
        {
            return this.Title;
        }
    }

    public class ServerInitiatedOperation : Operation
    {
        protected TaskCompletionSource<int> Tcs = new TaskCompletionSource<int>();

        protected void Complete([CanBeNull] Exception exception)
        {
            if (exception == null)
            {
                this.Tcs.TrySetResult(0);
            }
            else
            {
                this.Tcs.TrySetException(exception);
            }
        }

        public Task OnCompleted()
        {
            return this.Tcs.Task;
        }

        public string Cursor { get; protected set; }
    }

    public class DeleteFileOnServerOperation : Operation
    {
        private readonly LocalFileSystemEvent @event;
        private readonly ICloudStore cloudStore;

        public DeleteFileOnServerOperation(LocalFileSystemEvent @event, ICloudStore cloudStore)
        {
            this.@event = @event;
            this.cloudStore = cloudStore;
            this.Title = "Deleting : " + Path.GetFileName(@event.LocalPath);
        }

        public override async Task Execute()
        {
            await this.cloudStore.DeleteFile(this.@event.ServerPath, this.Cts.Token);
            this.IsCompleted = true;
        }
    }

    public class UploadChangedFileToServerOperation : Operation
    {
        private readonly LocalFileSystemEvent @event;
        private readonly ICloudStore cloudStore;
        private readonly RecentEventsStore recentEventsStore;

        public UploadChangedFileToServerOperation(LocalFileSystemEvent @event, ICloudStore cloudStore, RecentEventsStore recentEventsStore)
        {
            this.@event = @event;
            this.cloudStore = cloudStore;
            this.recentEventsStore = recentEventsStore;
            this.Title = "Upload due to change: " + Path.GetFileName(@event.LocalPath);
        }

        public override async Task Execute()
        {
            await this.cloudStore.UploadFile(this.@event.LocalPath, this.@event.ServerPath, this.Cts.Token);
            this.IsCompleted = true;
            this.recentEventsStore.PutRecentlyUploadedPath(this.@event.LocalPath);
        }
    }

    public class UploadFileToServerOperation : Operation
    {
        private readonly ICloudStore cloudStore;
        private readonly RecentEventsStore recentEventsStore;
        private readonly string localPath;
        private readonly string serverPath;

        public UploadFileToServerOperation(LocalFileSystemEvent @event, ICloudStore cloudStore, RecentEventsStore recentEventsStore)
        {
            this.localPath = @event.LocalPath;
            this.serverPath = @event.ServerPath;
            this.cloudStore = cloudStore;
            this.recentEventsStore = recentEventsStore;
            this.Title = "Upload : " + Path.GetFileName(@event.LocalPath);
        }

        public UploadFileToServerOperation(string serverPath, ICloudStore cloudStore)
        {
            this.localPath = serverPath.ToLocalPath();
            this.serverPath = serverPath;
            this.Title = "Upload : " + Path.GetFileName(serverPath);
            this.cloudStore = cloudStore;
        }

        public override async Task Execute()
        {
            await this.cloudStore.UploadFile(this.localPath, this.serverPath, this.Cts.Token);
            this.IsCompleted = true;
            this.recentEventsStore.PutRecentlyUploadedPath(this.localPath);
        }
    }

    public class RenameFileOnServerOperation : Operation
    {
        private readonly FileMovedLocallyEvent @event;
        private readonly ICloudStore cloudStore;

        public RenameFileOnServerOperation(FileMovedLocallyEvent @event, ICloudStore cloudStore)
        {
            this.@event = @event;
            this.cloudStore = cloudStore;
            this.Title = "Upload : " + Path.GetFileName(@event.LocalPath);
        }

        public override async Task Execute()
        {
            await this.cloudStore.RenameFile(this.@event.ServerPath, this.@event.NewServerPath, this.Cts.Token);
            this.IsCompleted = true;
        }
    }
    
    public class DownloadFileFromServer : ServerInitiatedOperation
    {
        private readonly ICloudStore cloudStore;
        private readonly string localPath;
        private readonly string serverPath;
        
        public DownloadFileFromServer(ServerFileSystemEvent @event, ICloudStore cloudStore)
        {
            this.localPath = @event.LocalPath;
            this.serverPath = @event.ServerPath;
            this.Title = "Download : " + Path.GetFileName(@event.LocalPath);
            this.cloudStore = cloudStore;
            this.Cursor = @event.DeltaCursor;
        }

        public override async Task Execute()
        {
            try
            {
                await this.cloudStore.DownloadFile(this.serverPath, this.localPath, this.Cts.Token);
                this.Complete(null);
            }
            catch (Exception exception)
            {
                this.Complete(exception);
            }
            finally
            {
                this.IsCompleted = true;
            }
        }
    }

    public class DeleteFileLocally : ServerInitiatedOperation
    {
        private readonly ServerFileSystemEvent @event;

        public DeleteFileLocally(ServerFileSystemEvent @event)
        {
            this.@event = @event;
            this.Cursor = @event.DeltaCursor;
        }

        public async override Task Execute()
        {
            try
            {
                await Task.Factory.StartNew(() => File.Delete(this.@event.LocalPath));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            
            this.IsCompleted = true;
            this.Complete(null);
        }
    }
}