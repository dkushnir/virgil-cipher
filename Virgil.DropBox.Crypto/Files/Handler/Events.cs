namespace Virgil.DropBox.Crypto.Files.Handler
{
    using Local;
    using Server;

    public class LocalFileSystemEvent
    {
        public LocalFileSystemEvent()
        {

        }

        public LocalFileSystemEvent(LocalFile file)
        {
            this.LocalPath = file.LocalPath;
            this.ServerPath = file.LocalPath.ToServerPath();
        }

        public string LocalPath { get; set; }

        public string ServerPath { get; set; }
    }

    public class FileMovedLocallyEvent : LocalFileSystemEvent
    {
        public string NewLocalPath { get; set; }
        public string NewServerPath { get; set; }
    }

    public class FileDeletedLocallyEvent : LocalFileSystemEvent
    {
        public FileDeletedLocallyEvent(LocalFile file) : base(file)
        {
        }
    }

    public class FileCreatedLocallyEvent : LocalFileSystemEvent
    {
        public FileCreatedLocallyEvent(LocalFile file) : base(file)
        {
        }
    }

    public class FileChangedLocallyEvent : LocalFileSystemEvent
    {
        public FileChangedLocallyEvent(LocalFile file) : base(file)
        {
        }
    }
    
    public class ServerFileSystemEvent
    {
        public ServerFileSystemEvent()
        {
        }
        
        public ServerFileSystemEvent(ServerFile serverFile, string cursor)
        {
            this.ServerPath = serverFile.Path;
            this.LocalPath = serverFile.Path.ToLocalPath();
            this.DeltaCursor = cursor;
        }

        public string LocalPath { get; set; }
        public string ServerPath { get; set; }
        public string DeltaCursor { get; set; }
    }

    public class ServerFileAddedEvent : ServerFileSystemEvent
    {
        public ServerFileAddedEvent(ServerFile serverFile, string cursor) : base(serverFile, cursor)
        {
        }
    }

    public class ServerFileDeletedEvent : ServerFileSystemEvent
    {
        public ServerFileDeletedEvent(string serverPath, string cursor)
        {
            this.ServerPath = serverPath;
            this.LocalPath = serverPath.ToLocalPath();
            this.DeltaCursor = cursor;
        }
    }

    public class ServerFileChangedEvent : ServerFileSystemEvent
    {
        public ServerFileChangedEvent(ServerFile serverFile, string cursor) : base(serverFile, cursor)
        {
        }
    }
}