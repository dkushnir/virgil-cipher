namespace Virgil.DropBox.Crypto.Files.Handler
{
    using System;
    using System.Runtime.Caching;

    public class RecentEventsStore 
    {
        public void PutRecentlyUploadedPath(string localPath)
        {
            MemoryCache.Default.Add(localPath.ToLowerInvariant(), Guid.NewGuid().ToString(), DateTimeOffset.Now.AddSeconds(15));
        }

        public bool PathWasRecentlyUploaded(string localPath)
        {
            return MemoryCache.Default.Get(localPath.ToLowerInvariant()) != null;
        }
        
        public void RemovePath(string localPath)
        {
            var key = localPath.ToLowerInvariant();
            MemoryCache.Default.Remove(key); 
        }
    }
}