namespace Virgil.DropBox.Crypto.Files
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface ICloudStore
    {
        Task UploadFile(string localFileName, string serverFileName, CancellationToken token);
        Task DownloadFile(string serverFileName, string localFileName, CancellationToken token);
        Task RenameFile(string oldServerFileName, string newServerFileName, CancellationToken token);
        Task DeleteFile(string serverFileName, CancellationToken token);
    }
}