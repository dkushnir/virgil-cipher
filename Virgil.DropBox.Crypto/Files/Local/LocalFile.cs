namespace Virgil.DropBox.Crypto.Files.Local
{
    using System;
    using System.IO;

    public class LocalFile
    {
        public LocalFile(string path)
        {
            this.LocalPath = path;
            this.Bytes = 0;

            try
            {
                var fileInfo = new FileInfo(path);
                this.Bytes = fileInfo.Length;
                this.Modified = fileInfo.LastWriteTimeUtc;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        public string LocalPath { get; }
        public long Bytes { get; }
        
        public DateTime Modified { get; }

        public override string ToString()
        {
            return $"{this.LocalPath.ToServerPath()} [{this.Bytes}] ({this.Modified.ToShortTimeString()})";
        }
    }
}