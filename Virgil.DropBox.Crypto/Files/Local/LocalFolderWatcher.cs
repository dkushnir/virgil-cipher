namespace Virgil.DropBox.Crypto.Files.Local
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reactive.Linq;

    public class LocalFolderWatcher
    {
        public const string TemporaryFileExtension = ".temporary";
        private readonly LocalFolder localFolder;
        private readonly FileSystemWatcher fileSystemWatcher;

        public LocalFolderWatcher(LocalFolder localFolder)
        {
            this.localFolder = localFolder;
            this.fileSystemWatcher = new FileSystemWatcher(localFolder.Folder)
            {
                IncludeSubdirectories = true,
                InternalBufferSize = 1024 * 256,
                NotifyFilter =
                    NotifyFilters.CreationTime |
                    NotifyFilters.DirectoryName |
                    NotifyFilters.FileName |
                    NotifyFilters.LastWrite |
                    NotifyFilters.Size |
                    NotifyFilters.Attributes
            };
        }
        
        public void Start()
        {
            this.localFolder.Init();

            var created = Observable.FromEventPattern<FileSystemEventArgs>(this.fileSystemWatcher, "Created");
            var changed = Observable.FromEventPattern<FileSystemEventArgs>(this.fileSystemWatcher, "Changed");

            var merged = created.Merge(changed)
                .Where(it => !it.EventArgs.FullPath.ToLowerInvariant().EndsWith(TemporaryFileExtension))
                
                .Buffer(TimeSpan.FromSeconds(2))
                .Where(it => it.Count > 0)
                .Select(eventPatterns =>
                {
                    return eventPatterns
                    .GroupBy(x => x.EventArgs.FullPath)
                    .Select(x =>
                    {
                        return x.FirstOrDefault(j => j.EventArgs.ChangeType == WatcherChangeTypes.Created) ??
                               x.FirstOrDefault();
                    });

                })
                .SelectMany(it => it.ToArray());

            merged.Subscribe(input => this.Handle(input.EventArgs));

            Observable.FromEventPattern<RenamedEventArgs>(this.fileSystemWatcher, "Renamed")
               //.Where(it => !it.EventArgs.OldFullPath.ToLowerInvariant().EndsWith(TemporaryFileExtension))
               .Subscribe(input => this.Handle(input.EventArgs));

            Observable.FromEventPattern<FileSystemEventArgs>(this.fileSystemWatcher, "Deleted")
                .Where(it => !it.EventArgs.FullPath.ToLowerInvariant().EndsWith(TemporaryFileExtension))
                .Subscribe(input => this.Handle(input.EventArgs));

            this.fileSystemWatcher.EnableRaisingEvents = true;
        }

        public void Stop()
        {
            this.fileSystemWatcher.EnableRaisingEvents = false;
        }

        private void Handle(FileSystemEventArgs args)
        {
            this.localFolder.Handle(args);
        }
    }
}