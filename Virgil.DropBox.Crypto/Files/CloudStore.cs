namespace Virgil.DropBox.Crypto.Files
{
    using System;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using DropNetRT;
    using DropNetRT.Models;
    using Encryption;
    using Local;

    public class CloudStore : ICloudStore
    {
        private readonly DropNetClient client;
        private readonly EncryptionCredentials credentials;
        private readonly LogRepository logRepository;

        public CloudStore(DropNetClient client, EncryptionCredentials credentials)
        {
            this.client = client;
            this.credentials = credentials;

            this.logRepository = new LogRepository();
        }

        const int BufferSize = 1024 * 512;
        
        public async Task UploadFile(string localFileName, string serverFileName, CancellationToken token)
        {
            var fileInfo = new FileInfo(localFileName);
            var lastWriteTime = fileInfo.LastWriteTimeUtc;

            using (var fileStream = new FileStream(localFileName,
                    FileMode.Open, FileAccess.Read, FileShare.Read,BufferSize, FileOptions.Asynchronous))
            {
                var encryptor = new CipherStreamEncryptor(fileStream);
                var contentInfo = encryptor.Init(this.credentials.RecepientId, this.credentials.PublicKey, BufferSize);
                
                var result = await this.client.UploadSessionStartAsync(contentInfo, token);

                Metadata metadata = new Metadata();

                while (encryptor.HasMore())
                {
                    var chunk = await encryptor.GetChunk();
                    
                    if (encryptor.HasMore())
                    {
                        result = await this.client.UploadSessionAppendAsync(chunk, result, token);
                    }
                    else
                    {
                        result = await this.client.UploadSessionAppendAsync(chunk, result, token);
                        var path = Path.GetDirectoryName(serverFileName);
                        var fileName = Path.GetFileName(serverFileName);
                        metadata = await this.client.UploadSessionFinishAsync(path, fileName, result, token);
                    }
                }

                var hashes = encryptor.GetHashes();

                this.logRepository.AddOrReplace(new LogEntry
                {
                    Hashes = hashes,
                    LocalPath = localFileName,
                    ServerPath = serverFileName,
                    ServerModified = metadata.UTCDateClientMtime,
                    LocalModified = lastWriteTime
                });
            }
        }
        
        public async Task DownloadFile(string serverFileName, string localFileName, CancellationToken token)
        {
            var tempLocalName = localFileName + LocalFolderWatcher.TemporaryFileExtension;
            try
            {
                var metadata = await this.client.GetMetaDataNoList(serverFileName);
                var stream = await this.client.GetFileStreamAsync(serverFileName, token);

                using (stream)
                using (var cipherStreamDecryptor = new CipherStreamDecryptor(stream))
                {
                    await cipherStreamDecryptor.Init(this.credentials.RecepientId, this.credentials.PrivateKey);

                    var localDir = Path.GetDirectoryName(localFileName);
                    if (localDir != null && !Directory.Exists(localDir))
                    {
                        Directory.CreateDirectory(localDir);
                    }

                    using (var dest = new FileStream(
                        tempLocalName, FileMode.Create, FileAccess.ReadWrite,
                        FileShare.Read, BufferSize, FileOptions.Asynchronous))
                    {
                        while (cipherStreamDecryptor.HasMore())
                        {
                            var chunk = await cipherStreamDecryptor.GetChunk();
                            await dest.WriteAsync(chunk, 0, chunk.Length, token);
                        }

                        await dest.FlushAsync(token);
                    }

                    File.Move(tempLocalName, localFileName);

                    var hashes = cipherStreamDecryptor.GetHashes();
                    var entry = this.logRepository.GetByLocalPath(localFileName);
                    if (entry == null || entry.Hashes != hashes)
                    {
                        this.logRepository.AddOrReplace(new LogEntry
                        {
                            Hashes = hashes,
                            LocalPath = localFileName,
                            ServerPath = serverFileName,
                            ServerModified = metadata.UTCDateClientMtime,
                            LocalModified = new FileInfo(localFileName).LastWriteTimeUtc
                        });
                    }
                }
            }
            finally 
            {
                try
                {
                    File.Delete(tempLocalName);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }
            }
        }

        public async Task RenameFile(string oldServerFileName, string newServerFileName, CancellationToken token)
        {
            await this.client.Move(oldServerFileName, newServerFileName, token);
        }

        public async Task DeleteFile(string serverFileName, CancellationToken token)
        {
            await this.client.Delete(serverFileName, token);
        }

    }
}