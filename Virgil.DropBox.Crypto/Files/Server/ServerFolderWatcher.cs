namespace Virgil.DropBox.Crypto.Files.Server
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using DropNetRT;
    using DropNetRT.Models;
    using Properties;

    public class ServerFolderWatcher
    {
        private readonly ServerFolder serverFolder;
        private readonly DropNetClient client;

        private readonly CancellationToken cancellationToken;
        private readonly CancellationTokenSource cancellationTokenSource;

        public ServerFolderWatcher(DropNetClient client, ServerFolder serverFolder)
        {
            this.serverFolder = serverFolder;
            this.client = client;
            this.cancellationTokenSource = new CancellationTokenSource();
            this.cancellationToken = this.cancellationTokenSource.Token;
        }

        private async Task Init()
        {
            var folderStructure = await this.GetDelta("");
            this.serverFolder.Init(folderStructure);
        }

        private async Task<Delta> GetDelta(string cursor)
        {
            var result = new List<DeltaPage>();

            var dp = await this.client.GetDelta(cursor, this.cancellationToken);
            result.Add(dp);
            while (dp.Has_More)
            {
                dp = await this.client.GetDelta(cursor, this.cancellationToken);
                result.Add(dp);
            }

            return new Delta(result, dp.Cursor);
        }

        public async Task Start()
        {
            await this.Init();

#pragma warning disable 4014
            Task.Factory.StartNew(this.CloudWatcher, TaskCreationOptions.LongRunning);
#pragma warning restore 4014
        }

        public void Stop()
        {
            this.cancellationTokenSource.Cancel();
        }

        private async void CloudWatcher()
        {
            try
            {
                await this.HandleDelta();

                while (true)
                {
                    var longpollDeltaResult = await this.client.GetLongpollDelta(Settings.Default.DeltaCursor, this.cancellationToken);
                    if (longpollDeltaResult.Changes)
                    {
                        await this.HandleDelta();
                    }

                    await Task.Delay(TimeSpan.FromSeconds(longpollDeltaResult.Backoff), this.cancellationToken);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private async Task HandleDelta()
        {
            var delta = await this.GetDelta(Settings.Default.DeltaCursor);


            await this.serverFolder.HandleDelta(delta);


            Settings.Default.DeltaCursor = delta.Cursor;
            Settings.Default.Save();
        }
    }
}