﻿namespace Virgil.DropBox.Crypto.Files.Server
{
    using System.Collections.Generic;
    using System.Linq;
    using DropNetRT.Models;

    public class Delta
    {
        public Delta(IEnumerable<DeltaPage> deltaPages, string cursor)
        {
            this.Cursor = cursor;
            this.Entries = deltaPages.SelectMany(it => it.Entries).ToList();
        }
        
        public string Cursor { get; }
        public List<DeltaEntry> Entries { get; }
    }
}