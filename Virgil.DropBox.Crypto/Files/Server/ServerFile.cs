namespace Virgil.DropBox.Crypto.Files.Server
{
    using System;
    using DropNetRT.Models;

    public class ServerFile
    {
        public string Path { get; set; }
        public long Bytes { get; set; }
        public DateTime ServerModified { get; set; }
        public DateTime ClientModified { get; set; }
        
        public ServerFile(DeltaEntry entry)
        {
            this.Path = entry.Path;
            this.Bytes = entry.MetaData.Bytes;
            this.ServerModified = entry.MetaData.UTCDateModified;
            this.ClientModified = entry.MetaData.UTCDateClientMtime;
        }

        public override string ToString()
        {
            return $"{this.Path} [{this.Bytes}] ({this.ServerModified.ToShortTimeString()}) ({this.ClientModified.ToShortTimeString()})";
        }
    }
}