namespace Virgil.DropBox.Crypto.Files.Server
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using Handler;

    public class ServerFolder
    {
        private readonly IServerEventListener listener;

        public ObservableCollection<ServerFile> Files { get; set; } = new ObservableCollection<ServerFile>();

        public ServerFolder(IServerEventListener listener)
        {
            this.listener = listener;
        }

        public void Init(Delta delta)
        {
            var serverFiles = delta.Entries
                .Where(it => !it.MetaData.IsDirectory)
                .Select(it => new ServerFile(it));

            foreach (var serverFileItem in serverFiles)
            {
                this.Files.Add(serverFileItem);
            }
            
            Console.WriteLine("+ ================================================ +");

            foreach (var fileItem in this.Files)
            {
                Console.WriteLine($"{fileItem.Path} [{fileItem.Bytes}]");
            }

            this.listener.OnInitialized(this, delta.Entries);
        }

        public async Task HandleDelta(Delta delta)
        {
            var entries = delta.Entries;
            var nullMetadata = entries.Where(it => it.MetaData == null);
            var deltaEntries = entries.Where(it => it.MetaData != null && !it.MetaData.IsDirectory).ToList();
            var toDelete = deltaEntries.Where(it => it.MetaData.IsDeleted).Union(nullMetadata);

            this.listener.Start(delta.Cursor);

            foreach (var deltaEntry in toDelete)
            {
                var fileItem = this.Files.FirstOrDefault(it => it.Path == deltaEntry.Path);

                if (fileItem != null)
                {
                    this.Files.Remove(fileItem);
                }
                
                this.listener.Add(new ServerFileDeletedEvent(deltaEntry.Path, delta.Cursor));
                Console.WriteLine($"Deleted {fileItem}");
            }

            var toAdd = deltaEntries
                .Where(it => !it.MetaData.IsDirectory);

            foreach (var deltaEntry in toAdd)
            {
                var fileItem = this.Files.FirstOrDefault(it => it.Path == deltaEntry.Path);

                if (fileItem == null)
                {
                    var item = new ServerFile(deltaEntry);
                    this.Files.Add(item);
                    Console.WriteLine($"Added {item}");// added
                    this.listener.Add(new ServerFileAddedEvent(item, delta.Cursor));
                }
                else
                {
                    fileItem.Bytes = deltaEntry.MetaData.Bytes;
                    Console.WriteLine($"Changed {fileItem}");
                    this.Files.Remove(fileItem);
                    this.Files.Add(fileItem);

                    this.listener.Add(new ServerFileChangedEvent(fileItem, delta.Cursor));
                }
            }

            await this.listener.WaitForAll(delta.Cursor);
        }
    }
}