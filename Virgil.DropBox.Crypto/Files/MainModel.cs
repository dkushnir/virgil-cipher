﻿namespace Virgil.DropBox.Crypto.Files
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Windows.Threading;
    using Annotations;
    using Handler;
    using Local;
    using Server;

    public class MainModel : INotifyPropertyChanged
    {
        private BigBrother bigBrother;
        private LocalFolder localFolder;
        private ServerFolder serverFolder;
        private List<Operation> operations;
        private List<ServerFile> serverFiles;
        private List<LocalFile> localFiles;

        public List<LocalFile> LocalFiles
        {
            get { return this.localFiles; }
            set
            {
                if (Equals(value, this.localFiles)) return;
                this.localFiles = value;
                this.OnPropertyChanged();
            }
        }

        public List<ServerFile> ServerFiles
        {
            get { return this.serverFiles; }
            set
            {
                if (Equals(value, this.serverFiles)) return;
                this.serverFiles = value;
                this.OnPropertyChanged();
            }
        }

        public List<Operation> Operations
        {
            get { return this.operations; }
            set
            {
                if (Equals(value, this.operations)) return;
                this.operations = value;
                this.OnPropertyChanged();
            }
        }


        public MainModel()
        {
            
        }

        public MainModel(BigBrother bigBrother, LocalFolder localFolder, ServerFolder serverFolder)
        {
            this.bigBrother = bigBrother;
            this.localFolder = localFolder;
            this.serverFolder = serverFolder;
            

            this.LocalFiles = localFolder.Files.ToList();
            this.ServerFiles = serverFolder.Files.ToList();
            this.Operations = bigBrother.Operations.ToList();

            localFolder.Files.CollectionChanged += (sender, args) =>
            {
                Dispatcher.CurrentDispatcher.Invoke(() => this.LocalFiles = localFolder.Files.ToList());
            };

            serverFolder.Files.CollectionChanged += (sender, args) =>
            {
                Dispatcher.CurrentDispatcher.Invoke(() => this.ServerFiles = serverFolder.Files.ToList());
            };

            bigBrother.Operations.CollectionChanged += (sender, args) =>
            {
                Dispatcher.CurrentDispatcher.Invoke(() => this.Operations = bigBrother.Operations.ToList());
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
