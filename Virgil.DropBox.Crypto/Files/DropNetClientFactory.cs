namespace Virgil.DropBox.Crypto.Files
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using DropNetRT;
    using DropNetRT.Models;
    using Encryption;

    public class DropNetClientFactory
    {
        public async Task<DropNetClient> GetInstance()
        {
            var client = new DropNetClient("d4x24xt4v1s4xfr", "72u29o1a9ps1h2l", "jmqjz1h7cm5dzvuq", "nkuxc3cakd6mext")
            {
                UseSandbox = true,
                Timeout = TimeSpan.FromHours(1)
            };

            if (client.UserLogin == null)
            {
                client.UserLogin = await this.GetUserLogin(client);
            }

            return client;
        }

        private async Task<UserLogin> GetUserLogin(DropNetClient client)
        {
            var requestToken = await client.GetRequestToken();
            var url = client.BuildAuthorizeUrl(requestToken, "http://localhost:5000/Home/Auth");
            var completion = new TaskCompletionSource<Tuple<string, string>>();
            var thread = new Thread(() =>
            {
                try
                {
                    var app = new Application();
                    var login = new LoginForm(url);

                    app.Run(login);
                    if (login.Result)
                    {
                        completion.TrySetResult(Tuple.Create(login.AccessToken, login.Uid));
                    }
                    else
                    {
                        completion.TrySetCanceled();
                    }
                }
                catch (Exception e)
                {
                    completion.TrySetException(e);
                }
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();

            await completion.Task;
            return await client.GetAccessToken();
        }
    }
}