﻿namespace DropNetRT
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using Exceptions;
    using Extensions;
    using HttpHelpers;
    using Models;
    using Newtonsoft.Json;

    public partial class DropNetClient
    {
        public async Task<ChunkedUploadResponse> UploadSessionStartAsync(byte[] chunk, 
            CancellationToken cancellationToken)
        {
            var request = MakeChunkedUploadPutRequest(0);

            HttpContent content = new ByteArrayContent(chunk, 0, chunk.Length);

            try
            {
                var response = await _httpClient.PutAsync(request.RequestUri, content, cancellationToken);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new DropboxException(response);
                }

                var body = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ChunkedUploadResponse>(body);
            }
            catch (AggregateException)
            {
                throw;
            }
            catch (DropboxException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DropboxException(ex);
            }
        }

        public async Task<ChunkedUploadResponse> UploadSessionAppendAsync(byte[] chunk, ChunkedUploadResponse lastResponse, 
            CancellationToken cancellationToken)
        {
            if (lastResponse == null)
            {
                throw new ArgumentNullException(nameof(lastResponse));
            }

            var offset = lastResponse.Offset;
            var uploadId = lastResponse.UploadId;

            var request = MakeChunkedUploadPutRequest(offset, uploadId);

            HttpContent content = new ByteArrayContent(chunk, 0, chunk.Length);

            try
            {
                var response = await _httpClient.PutAsync(request.RequestUri, content, cancellationToken);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new DropboxException(response);
                }

                var body = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ChunkedUploadResponse>(body);
            }
            catch (AggregateException)
            {
                throw;
            }
            catch (DropboxException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DropboxException(ex);
            }
        }

        public async Task<Metadata> UploadSessionFinishAsync(
            string path, string filename, 
            ChunkedUploadResponse lastResponse,  
            CancellationToken cancellationToken)
        {
            try
            {
                var commitRequest = this.MakeChunkedUploadCommitRequest
                    (path, filename, lastResponse.UploadId);

                var response = await this._httpClient.PostAsync(commitRequest.RequestUri, null, cancellationToken);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new DropboxException(response);
                }

                var responseBody = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<Metadata>(responseBody);
            }
            catch (AggregateException)
            {
                throw;
            }
            catch (DropboxException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DropboxException(ex);
            }
        }
        
        public async Task<Stream> GetFileStreamAsync(string path, CancellationToken cancellationToken)
        {
            var request = MakeGetFileRequest(path);

            var response = await _httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken);
            
            if (HttpStatusCode.OK != response.StatusCode)
            {
                throw new DropboxException(response.StatusCode);
            }

            return await response.Content.ReadAsStreamAsync();
        }
    }
}